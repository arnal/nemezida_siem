import 'package:flutter/material.dart';

class Helpers{
  static Color greenColor = Color(0xff91E972);
  static Color yellowColor = Color(0xffECF57A);
  static Color redColor = Color(0xffEC7171);
  static Color blackColor = Color(0xff212121);
  static Color greyColor = Color(0xffEFEDED);

  static TextStyle  hintStyle = TextStyle(
    fontSize: 16.0,
    color: greyColor,
  );

}