import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:nemezida_siem/helpers/helpers.dart';
import 'package:nemezida_siem/models/result_model.dart';
import 'package:nemezida_siem/screens/item_card.dart';

class MainScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    var listValue1 = Results(
        text:
            'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Tortor, pellentesque laoreet sed tempor dolor, eros, potenti.Tortor, pellentesque laoreet sed.',
        level: '1', color: Helpers.redColor);
    var listValue2 = Results(
        text:
            'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Tortor, pellentesque laoreet sed tempor dolor, eros, potenti.Tortor, pellentesque laoreet sed.',
        level: '1', color: Helpers.redColor);
    var listValue3 = Results(
        text:
            'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Tortor, pellentesque laoreet sed tempor dolor, eros, potenti.Tortor, pellentesque laoreet sed.',
        level: '5', color: Helpers.greenColor);
    var listValue4 = Results(
        text:
            'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Tortor, pellentesque laoreet sed tempor dolor, eros, potenti.Tortor, pellentesque laoreet sed.',
        level: '3', color: Helpers.yellowColor);
    var listValues = <Results>[];
      listValues.add(listValue1);
      listValues.add(listValue2);
      listValues.add(listValue3);
      listValues.add(listValue4);

    return Scaffold(
      appBar: null,
      body: Padding(
        padding: const EdgeInsets.only(top: 60.0),
        child: ListView.builder(
          physics: ScrollPhysics(),
          shrinkWrap: true,
          itemCount: listValues.length,
          itemBuilder: (BuildContext context, int index) {
            return ItemCard(listValues[index]);
          },
        ),
      ),
    );
  }
}
