import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:nemezida_siem/helpers/helpers.dart';

class AuthScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: null,
      body: Center(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            SvgPicture.asset(
              'assets/images/logo.svg',
              fit: BoxFit.cover,
            ),
            SizedBox(height: 40),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 50),
              child: Column(
                children: [
                  Container(
                    height: 40,
                    decoration: BoxDecoration(
                      color: Helpers.greyColor,
                      borderRadius: const BorderRadius.all(const Radius.circular(10)),
                    ),
                    child: Padding(
                      padding: const EdgeInsets.symmetric(vertical: 10, horizontal: 9),
                      child: TextFormField(
                          decoration: InputDecoration(
                            hintStyle: TextStyle(
                                fontWeight: FontWeight.w400, fontSize: 12.0, color: Helpers.blackColor),
                            border: InputBorder.none,
                            hintText: 'Логин',
                          ),
                          // validator: Helpers.validatePassword,
                          onSaved: (value) {
                            //  _user.password = value;
                          }),
                    ),
                  ),
                  SizedBox(height: 16),
                  Container(
                    height: 40,
                    decoration: BoxDecoration(
                      color: Helpers.greyColor,
                      borderRadius: const BorderRadius.all(const Radius.circular(10)),
                    ),
                    child: Padding(
                      padding: const EdgeInsets.symmetric(vertical: 10, horizontal: 9),
                      child: TextFormField(
                          decoration: InputDecoration(
                            hintStyle: TextStyle(
                                fontWeight: FontWeight.w400, fontSize: 12.0, color: Helpers.blackColor),
                            border: InputBorder.none,
                            hintText: 'Пароль',
                          ),
                          obscureText: true,
                          obscuringCharacter: '*',
                          // validator: Helpers.validatePassword,
                          onSaved: (value) {
                            //  _user.password = value;
                          }),
                    ),
                  ),
                  SizedBox(height: 16),
                  ElevatedButton(
                      onPressed: () {},
                      child: Container(
                        width: double.infinity,
                        child: Center(
                          child: Text(
                            'Войти',
                            style: TextStyle(fontWeight: FontWeight.w700),
                          ),
                        ),
                      ))
                ],
              ),
            ),


          ],
        ),
      ),
    );
  }
}
