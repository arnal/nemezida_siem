import 'package:flutter/material.dart';
import 'package:nemezida_siem/helpers/helpers.dart';
import 'package:nemezida_siem/models/result_model.dart';

class ItemCard extends StatelessWidget {
  final Results listValue;

  ItemCard(this.listValue);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 10.0, vertical: 3),
      child: Card(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(15.0),
        ),
        color: Helpers.greyColor,
        child: Padding(
          padding: const EdgeInsets.all(17.0),
          child: Row(
            children: [
              Expanded(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      'Уровень опасности:',
                      textAlign: TextAlign.start,
                      style: TextStyle(
                          fontWeight: FontWeight.w700, fontSize: 14.0, color: Helpers.blackColor),
                    ),
                    SizedBox(height: 9),
                    Text(
                      listValue.text,
                      style: TextStyle(
                          fontWeight: FontWeight.w400, fontSize: 10.0, color: Colors.black),
                      maxLines: 5,
                    ),
                  ],
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(0.0),
                child: Container(
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.all(Radius.circular(12)), color: listValue.color),
                  child: Padding(
                    padding: const EdgeInsets.symmetric(vertical: 9.0, horizontal: 21),
                    child: Center(
                        child: Text(
                      listValue.level,
                      style: TextStyle(
                          fontWeight: FontWeight.w700, fontSize: 42, color: Helpers.blackColor),
                    )),
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
