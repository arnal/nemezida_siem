import 'package:flutter/material.dart';

class Results {
   String text;
   String level;
   Color color;

  Results({ this.text,  this.level,  this.color});

  Results.fromJson(Map<String, dynamic> json) {
    text = json['text'];
    level = json['level'];
    color = json['color'];
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['text'] = text;
    data['level'] = level;
    data['color'] = color;
    return data;
  }
}

